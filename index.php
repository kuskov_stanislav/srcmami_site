
<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */
function myheader(){
    $header = <<<EOT
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SRC MAMI Студенческое робототехническое сообщество</title>
    <link rel="icon" type="image/png" href="template/img/favicon.png" />
    <!-- Bootstrap Core CSS -->
    <link href="template/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="template/css/logo-nav.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <img src="template/img/logo.png" height="45px" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="/" class="glyphicon glyphicon-home" style="color: #08a0ff"></a>
                    </li>

                        <li>
                        <a href="/about">О нас</a>
                        </li>
                    <li>
                        <a href="/projects">Проекты</a>
                    </li>
                    <li>
                        <a href="/contacts">Контакты</a>
                    </li>
                    <li>
                        <a href="/reg">РЕГИСТРАЦИЯ НА ПРОЕКТ</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
EOT;
    return $header;
}

function myfoother(){
    $foother = <<<EOT
        <!-- jQuery -->
    <script src="template/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="template/js/bootstrap.min.js"></script>

</body>

</html>
EOT;
    return $foother;
}

echo myheader();
include_once("analyticstracking.php");
// GET route
$app->get(
    '/',
    function () {
        $template = <<<EOT


    <!-- Page Content -->

    <header id="top" class="header">
        <div class="text-vertical-center">
            <h1 style="font-size: 40pt">SRC MAMI</h1>
            <h5>СТУДЕНЧЕСКОЕ РОБОТОТЕХНИЧЕСКОЕ СООБЩЕСТВО</h5>
            <h5>МОСКОВСКИЙ ГОСУДАРСТВЕННЫЙ
МАШИНОСТРОИТЕЛЬНЫЙ УНИВЕРСИТЕТ (МАМИ)</h5>
            <br>
            <a href="/about" class="btn btn-dark btn-lg" style="min-width: 150px">О нас</a>
        </div>
    </header>


    <!-- /.container -->
EOT;
        //echo myheader();
        echo $template;
        //echo myfoother();
    }
);
$app->get(
    '/events',
    function () {
        $template = <<<EOT


    <!-- Page Content -->

   <div class="empty" style="padding-top: 100px;"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <h1>События</h1>
            <h3>Страница в разработке</h3>
        </div>
    </div>
</div>

    <!-- /.container -->
EOT;
        //echo myheader();
        echo $template;
        //echo myfoother();
    }
);
$app->get(
    '/about',
    function () {
        $template = <<<EOT


    <!-- Page Content -->
        <div class="empty" style="padding-top: 100px;"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>О нас</h1>
            </div>
            <div class="col-lg-12" style="text-align: center;">
                <p><b>Студенческое робототехническое сообщество SRC MAMI</b></p>
            </div>
            <div class="col-lg-12" style="text-align: justify;">
                <p style="text-indent: 1.5em;">Студенческое робототехническое сообщество SRC MAMI было сформировано из студентов направлений УТС и АИС, интересующихся робототехникой, мехатроникой и программированием.
                Лаборатория SRC MAMI распологается в ауд. Н-117. В настоящее время в лаборатории ведется работа над проектом "Carolo-cup".
                «Carolo-Сup» — это возможность для студенческих Команд самостоятельно разрабатывать и конструировать транспортное средство, оснащенное автоматической системой управления.
                Основная цель состоит в реализации наилучшей автоматической системы управления транспортным средством в различных смоделированных ситуациях,
                основанных на реальных условиях.
                </p>
                <hr width="100%">
            </div>

            <div class="col-lg-12" style="text-align: center;">
                <p><b>Наша команда 2015</b></p>
            </div>
            <div class="col-lg-12" style="text-align: center;">
                <div class="col-lg-4 col-md-6" style="text-align: center;">
                    <img src="template/img/karpenko.png" width="60%">
                    <p><b>Инженер, программист</b></p>
                    <p>Павел Карпенко</p>
                </div>
                <div class="col-lg-4 col-md-6" style="text-align: center;">
                    <img src="template/img/kuskov.png" width="60%">
                    <p><b>Инженер</b></p>
                    <p>Станислав Кусков</p>
                </div>
                <div class="col-lg-4 col-md-6" style="text-align: center;">
                    <img src="template/img/ivanova.png" width="60%">
                    <p><b>Менеджер, дизайнер</b></p>
                    <p>Кристина Иванова</p>
                </div>
                <div class="col-lg-4 col-md-6" style="text-align: center;">
                    <img src="template/img/pogrebnyak.png" width="60%">
                    <p><b>Программист</b></p>
                    <p>Наталья Погребняк</p>
                </div>
                <div class="col-lg-4 col-md-6" style="text-align: center;">
                    <img src="template/img/minenko.png" width="60%">
                    <p><b>Инженер</b></p>
                    <p>Игорь Миненко</p>
                </div>
                <div class="col-lg-4 col-md-6" style="text-align: center;">
                    <img src="template/img/chernyaev.png" width="60%">
                    <p><b>Программист</b></p>
                    <p>Артем Черняев</p>
                </div>
                <hr width="100%">
            </div>
        </div>
    </div>
    <!-- /.container -->
EOT;
        //echo myheader();
        echo $template;
        //echo myfoother();
    }
);
$app->get(
/**
 *
 */
    '/projects',
    function () {
        $template = <<<EOT
    <!-- Page Content -->
        <div class="empty" style="padding-top: 100px;"></div>
    <div class="container">
        <div class="row">
        <div class="col-lg-12"><h1>Проекты</h1></div>

            <div class="col-lg-12" style="text-align: center;">

                <div class="panel panel-default" style="border-radius: 0;">
                    <div class="panel-heading">
                        <h3 class="panel-title"><b>Проект Carolo-Сup</b></h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-6">
                            <p><b>Цель соревнования</b></p>
                            <p style="text-align: justify; text-indent: 1.5em;">
                            Соревнование для высших учебных заведений «Carolo-Сup» — это возможность для студенческих Команд самостоятельно разрабатывать и конструировать транспортное средство, оснащенное автоматической системой управления.
                Основная цель состоит в реализации наилучшей автоматической системы управления транспортным средством в различных смоделированных ситуациях,
                основанных на реальных условиях.
                            Соревнование проводится ежегодно и позволяет студентам продемонстрировать свои инженерные навыки группе экспертов, работающих в промышленности и научных кругах, а также сразиться с командами других университетов.
                            Организатором соревнования для высших учебных заведений «Carolo-Сup» выступает Брауншвейгский технический университет.
                            </p>
                          <hr width="100%">
                            <p><b>Задачи</b></p>
                            <p style="text-align: justify; text-indent: 1.5em;">
                            Студенческая Команда, выступая в роли автопроизводителя, разрабатывает концепцию экономически эффективного и энергосберегающего транспортного средства масштабом 1:10.
                            Команды, принимающие участие в соревновании, должны настроить свое транспортное средство таким образом, чтобы оно быстро и безошибочно выполняло предлагаемые задачи.
                            Затем разработанная концепция должна быть защищена членами Команды перед судьями на презентации.</p>
                            <hr width="100%"><p><a href="files/CoroloPlan.xlsx">Скачать календарный план проекта "Carolo-cup"</a></p>
                       <hr width="100%">
                        </div>
                        <div class="col-lg-6">
                            <div class="col-lg-12">
                                <script type="text/javascript" src="charts/jsapi"></script>
                                <script type="text/javascript">
                                      google.load("visualization", "1.1", {packages:["bar"]});
                                      google.setOnLoadCallback(drawChart);
                                      function drawChart() {
                                        var data = google.visualization.arrayToDataTable([
                                          ['ДАТА ПРОВЕДЕНИЯ', 'Количество мест', 'Место'],
                                          ['фев. 2015', 21, 16],
                                          ['фев. 2016', 0, 0],
                                          ['фев. 2017', 0, 0],
                                          ['фев. 2018', 0, 0]
                                        ]);

                                        var options = {
                                          chart: {
                                            title: 'График участия в соревнованиях "Carolo-cup"',
                                            subtitle: '',
                                          }
                                        };

                                        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                                        chart.draw(data, options);
                                      }
                                </script>

                                        <div id="columnchart_material" style="text-align: center"></div>
                            </div>

                            <div class="col-lg-12">
                            <hr width="100%">

                                <img src="template/img/car.jpg" width="90%">
                                <hr width="100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
EOT;
        //echo myheader();
        echo $template;
        //echo myfoother();

    }
);
$app->get(
/**
 *
 */
    '/contacts',
    function () {
        $template = <<<EOT
    <!-- Page Content -->
        <div class="empty" style="padding-top: 100px;"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Контакты</h1>
            </div>
            <div class="col-lg-12" style="text-align: center;">
                <div class="col-lg-4 col-md-6" style="text-align: center;">
                    <img src="template/img/iturralde.png" width="40%">
                    <p><b>Координатор проекта "Carolo-cup"</b></p>
                    <p>Пабло Итурралде</p>
                    <p>+7(915)252-39-97</p>
                    <p><a href="mailto:iturralde.p@gmail.com">iturralde.p@gmail.com</a></p>
                </div>
                <div class="col-lg-4 col-md-6" style="text-align: center;">
                    <img src="template/img/karpenko.png" width="40%">
                    <p><b>Руководитель проекта "Carolo-cup"</b></p>
                    <p>Павел Карпенко</p>
                    <p>+7(926)667-72-78</p>
                    <p><a href="mailto:mspavel001@gmail.com">mspavel001@gmail.com</a></p>
                </div>
                <div class="col-lg-4 col-md-6" style="text-align: center;">
                    <img src="template/img/kuskov.png" width="40%">
                    <p><b>Руководитель проекта "Carolo-cup"</b></p>
                    <p>Станислав Кусков</p>
                    <p>+7(926)335-83-13</p>
                    <p><a href="mailto:kuskov.stanislav@gmail.com">kuskov.stanislav@gmail.com</a></p>
                </div>
                <hr width="100%">
            </div>
            <div class="col-lg-12" style="text-align: center;">
                <p><b>Лаборатория SRC MAMI</b></p>
                <p>Телефон: +7 (495) 223-05-23, доб. 1364</p>
                <p>Адрес: Большая Семеновская ул., 38, Москва, 107023 АУД. Н-117</p>

            </div>
            </div>
    </div>
    <!-- /.container -->
EOT;
        //echo myheader();
        echo $template;
        //echo myfoother();
    }
);
$app->get(
    '/reg',
    function () {
        $template = <<<EOT
    <!-- Page Content -->
        <div class="empty" style="padding-top: 100px;"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Регистрация на проект "Carolo-cup"</h1>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="text-align: center;">Заполните форму регистрации</h3>
                    </div>
                    <div class="panel-body" style="text-align: center;">

                            <form class="form-horizontal" role="form" action="/reg" method="post">
                                <div class="form-group">
                                    <label for="inputText" class="col-sm-3 control-label">Расскажите о себе:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="inputText" placeholder="Что в проекте вам наиболее интересно?" name="about"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 control-label">Ваше имя:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="inputName"  placeholder="Введите Ваше имя" name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputWho" class="col-sm-3 control-label">Направление:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="inputWho" name="who">
                                            <option>Не знаю</option>
                                            <option>Механика</option>
                                            <option>Электроника</option>
                                            <option>Программирование</option>
                                            <option>Дизайн, САПР</option>
                                            <option>PR, Project management</option>
                                            <option>Реклама</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputAge" class="col-sm-3 control-label" style="color: #333;">Ваш номер телефона:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="inputAge"  placeholder="Введите Ваш номер телефона" name="phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-3 control-label" style="color: #333;">Ваша почта:</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="inputEmail"  placeholder="Введите Ваш адрес электронной почты" name="mail">
                                    </div>
                                </div>
                                <div class="form-group">
                                <div class="col-sm-9">
                                    </div>
                                <div class="col-sm-3">
                                <button type="submit" class="btn btn-info btn-lg btn-block" name="regform">Регистрация</button>
                                </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
EOT;
        //echo myheader();
        echo $template;
        //echo myfoother();

    }
);
// POST route
$app->post(
    '/reg',
    function () {
        if (isset($_POST['regform'])){
            if (!empty($_POST['name'])){
                include('classes/dbconfig.php');
                include('classes/reg.class.php');

                $reg = new ProjectReg();
                $reg->reg($_POST['about'],$_POST['name'],$_POST['phone'],$_POST['mail'],$_POST['who']);
                echo "<div class=\"col-lg-12\" style='text-align: justify'>
            Вы зарегистрированы на проект \"Carolo-cup\", дальнейшие указания Вы получите по почте, которую указали при регистрации
            <br>
            <a href='/'>На главную</a></div>";
            }

        } else {

        }
        //echo $_POST['who'];
    }

);

// PUT route
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);

// PATCH route
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});

// DELETE route
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
echo myfoother();
