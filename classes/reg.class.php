<?php
/**
 * Created by PhpStorm.
 * User: kuskov
 * Date: 30.07.2015
 * Time: 13:01
 */

class ProjectReg {
    function reg($about,$name,$phone,$mail,$who){
        $date=date('Y-d-m');
            $options = array(PDO:: MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
            $bdconnect = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD, $options);
            $sql = "INSERT INTO `reg`(`reg_id`,`reg_mail`,`reg_name`,`reg_phone`,`reg_text`,`reg_date`,`reg_who`) VALUES (NULL,:reg_mail,:reg_name,:reg_phone,:reg_about,:reg_date,:who)";
            $query = $bdconnect->prepare($sql);
            $query->bindValue(":reg_about", $about, PDO::PARAM_STR);
            $query->bindValue(":reg_name", $name, PDO::PARAM_STR);
            $query->bindValue(":reg_phone", $phone, PDO::PARAM_INT);
            $query->bindValue(":reg_mail", $mail, PDO::PARAM_STR);
            $query->bindValue(":who", $who, PDO::PARAM_STR);
            $query->bindValue(":reg_date", $date, PDO::PARAM_STR);
            $query->execute();
            $sql = null;
            $query = null;
            $bdconnect = null;
    }

}